import config

import MySQLdb
import traceback


class Database:

    TABLE_CANDIDATE_KEYS = {
        'celebrities':  ['rtid'],
        'genres':       ['genre'],
        'movies':       ['rtid'],
        'reviews':      ['author', 'content'],
        'roles':        ['celebrity', 'content', 'type'],
        'shows':        ['rtid'],
        'trailers':     ['rtid'],
        'users':        ['rtid'],
    }

    TABLE_PRIMARY_KEYS = {
        'celebrities':  ['person_id'],
        'contents':     ['content_id'],
        'genres':       ['genre_id'],
        'images':       ['image_id'],
        'movies':       ['content_id'],
        'persons':      ['person_id'],
        'playlists':    ['playlist_id'],
        'reviews':      ['review_id'],
        'roles':        ['role_id'],
        'seasons':      ['season_id'],
        'shows':        ['content_id'],
        'trailers':     ['trailer_id'],
        'users':        ['person_id'],
    }

    TABLE_INSERTIONS = {
        'celebrities': '''
            INSERT INTO celebrities VALUES (rtid, person_id);
        ''',
        'contents': '''
            INSERT INTO contents(released, summary, title, image)
            VALUES (released, summary, name, image);
        ''',
        'genre_content': '''
            INSERT INTO genre_content VALUES (genre_id, content_id);
        ''',
        'genres': '''
            INSERT INTO genres(genre) VALUES (genre);
        ''',
        'images': '''
            INSERT INTO images(data_type, data, full, icon, content)
            VALUES (dataType, data, full, icon, content);
        ''',
        'movies': '''
            INSERT INTO movies VALUES (boxOffice, rating, ratingFull, rtid, runtime, studio, content_id);
        ''',
        'persons': '''
            INSERT INTO persons(bio, birthday, birthplace, date_created, full_name, image)
            VALUES (bio, birthday, birthplace, dateCreated, name, image);
        ''',
        'playlists': '''
            INSERT INTO playlists VALUES ();
        ''',
        'reviews': '''
            INSERT INTO reviews(body, date_Created, rating, source_url, author, content)
            VALUES (body, dateCreated, rating, sourceUrl, author, content);
        ''',
        'roles': '''
            INSERT INTO roles(credit, type, celebrity, content)
            VALUES (credit, type, celebrity, content);
        ''',
        'seasons': '''
            INSERT INTO seasons(consensus, episodes, number, title, year, image, show_id)
            VALUES (consensus, episodes, number, name, year, image, show);
        ''',
        'shows': '''
            INSERT INTO shows VALUES (last_aired, network, rating_count, rating_value, rtid, content_id);
        ''',
        'trailers': '''
            INSERT INTO trailers(rtid, runtime, source_url, title, content, image)
            VALUES (rtid, runtime, url, name, content, image);
        ''',
        'users': '''
            INSERT INTO users(active, email, pass, permission, rtid, verified, views, person_id, not_interested, want_to_see)
            VALUES (active, email, password, permission, rtid, verified, views, person_id, not_interested, want_to_see);
        ''',
    }

    def __init__(self, debug=False):
        self.db = MySQLdb.connect(
            host    = config.DB_HOST,
            user    = config.DB_USER,
            passwd  = config.DB_PASS,
            db      = config.DB_NAME,
        )
        self.debug = debug

    def close(self):
        self.db.close()

    def find(self, table, values, isPK=True):
        query = self.create_query(table, values, isPK)

        if query is None:
            return None

        try:
            cursor = self.db.cursor()
            cursor.execute(query)
            rows = cursor.fetchall()

            if len(rows) == 0:
                return None
            elif len(rows) > 1:
                print('ERROR: find(%s, %s) got %d results' % (table, values, len(rows)))
                return None

            fields = map(lambda x:x[0], cursor.description)
            result = dict(zip(fields, rows[0]))
            return result.get(Database.TABLE_PRIMARY_KEYS.get(table)[0])
        except UnicodeError:
            return None
        except Exception:
            print('ERROR: find(%s, %s) exception occured for query "%s"' % (table, values, query))
            traceback.print_exc()

            if self.debug:
                raise Exception
            return None

    def insert(self, table, data):
        statement = self.create_transaction(table, data, Database.TABLE_INSERTIONS)

        if statement is None:
            return None

        try:
            cursor = self.db.cursor()
            cursor.execute(statement)
            self.db.commit()
            return cursor.lastrowid
        except UnicodeError:
            return None
        except Exception:
            print('ERROR: insert(%s, data) exception occurred for insertion "%s"' % (table, statement))
            traceback.print_exc()

            if self.debug:
                raise Exception
            return None

    def create_query(self, table, values, isPK):
        keys = None

        for i in range(len(values)):
            if type(values[i]) is str:
                values[i] = "'" + values[i] + "'"

        if isPK:
            keys = Database.TABLE_PRIMARY_KEYS.get(table, None)
        else:
            keys = Database.TABLE_CANDIDATE_KEYS.get(table, None)

        if keys is None:
            return None

        if len(keys) != len(values):
            print('ERROR: create_query(%s, %s, %s): len(keys) did not match len(values)'
                % (table, keys, values))
            return None

        query = 'SELECT * FROM {0} WHERE '.format(table)

        for key, value in zip(keys, values):
            query += '{0} = {1} AND '.format(key, value)

        return query[:-5] + ';'

    def create_transaction(self, table, data, statements):
        statement = statements.get(table, None)

        if statement is None:
            print('ERROR: Table was invalid: create_transaction(%s, data)' % table)
            return None

        start   = statement.find('VALUES (')+8
        end     = statement.rfind(')')

        keys    = statement[start:end].split(', ')
        values  = list()

        for key in keys:
            value   = getattr(data, key, None)

            if value is None:
                value = 'NULL'
            elif type(value) is str and not value.endswith('()'):
                value = "'" + self._convert_str(value) + "'"

            values.append(str(value))

        return statement[:start] + ', '.join(values) + ');'

    # Convert strings to acceptable format for insertion
    def _convert_str(self, string):
        out = ''

        for c in string:
            if c in ["'", '"']:
                out += '\\'
            out += c

        return out