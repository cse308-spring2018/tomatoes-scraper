import config, data
from db import Database

from bs4 import BeautifulSoup
from datetime import datetime
from difflib import SequenceMatcher
from json import (
	dump,
	loads,
)
from random import randint
import re, requests, traceback


# Helper to create the specified Scraper
def get_scraper(class_name):
	return eval(class_name.capitalize() + 'Scraper')


class FailedToScrapeContentException(Exception):
	pass


class Scraper:
	BASE_URL 	= 'https://www.rottentomatoes.com/'
	WHITESPACE 	= '_'
	URL 		= 'bad_url/'
	NOT_FOUND 	= '404 - Not Found'

	DATA_CLASS	= data.Data
	TABLE_NAME	= None

	@classmethod
	def fetch_soup(cls, source_url):
		''' Fetch source url page as soup'''
		try:
			html = requests.get(source_url, config.HEADERS).content
		except:
			return None

		soup = BeautifulSoup(html, 'html.parser')

		# Check for 404
		if cls._check_not_found(soup):
			return None

		return soup

	@classmethod
	def run(cls, name, url_name, formatted=False, overwrite=False, debug=False):
		''' Begin scraping and call _run subroutine '''

		name 	= cls._strip_whitespace(name)
		rtid 	= url_name if formatted else cls._format_id(url_name)
		url 	= cls._source_url(rtid)

		if debug:
			print('Attempting to find data for "%s"' % url_name)

		if rtid is None:
			return None

		# Check if this class already exists
		_id = cls.DATA_CLASS.get(rtid)

		if _id is not None:
			return _id

		response = cls.DATA_CLASS()
		soup = cls.fetch_soup(url)

		response.name = name
		response.rtid = rtid

		# Check if the page was found
		if soup is None:
			return None

		return cls._run(response, soup)

	# Function stubs

	@classmethod
	def _run(cls, obj, soup):
		''' Run the scraper '''
		raise NotImplementedError

	# Helpers

	@classmethod
	def _check_not_found(cls, soup):
		''' Check for RT 404 in main body of page '''
		return Scraper.NOT_FOUND in soup.get_text()

	@classmethod
	def _format_id(cls, name):
		name = cls._to_snake_case(name).lower()
		name = re.sub(r'\W+', '', name)
		name = name.replace('_', cls.WHITESPACE)
		return name

	@classmethod
	def _format_attribute(cls, attr_name):
		attr_name = cls._to_snake_case(attr_name).lower()
		attr_name = re.sub(r'\W+', '', attr_name)
		return attr_name

	@classmethod
	def _strip_whitespace(cls, string):
		return ' '.join(string.split())

	@classmethod
	def _to_snake_case(cls, string):
		string = cls._strip_whitespace(string)
		string = string.replace(' ', '_')
		return string

	@classmethod
	def _source_url(cls, rtid):
		return Scraper.BASE_URL + cls.URL + rtid


class ContentScraper(Scraper):

	@classmethod
	def _scrape_person(cls, content_id, name, url, role_type, credit=None):
		if name is None:
			cls.print_step(2, 'FAILED %s %s' % (role_type, str(name)))
			return 0

		# TODO: get Role data and update credit if credit is not None
		if role_type == 'ACTOR' and credit is None:
			return 0

		if url is None:
			celeb_id = create_empty_celebrity(name)
		else:
			rtid = url.rsplit('/', 1)[-1]
			if rtid is None:
				return 0
			celeb_id = CelebrityScraper.run(name, rtid, formatted=True)

		if celeb_id is None:
			return 0

		# Check if a Role matching this celeb, content, and type exists already
		role = data.Role.get([celeb_id, content_id, role_type])

		if role is not None:
			return 0

		# Create the Role
		role = data.Role()

		role.content 	= content_id
		role.celebrity 	= celeb_id
		role.type 		= role_type
		role.credit		= credit

		# Save the Role
		role.commit()

		cls.print_step(2, '%s as %s' % (name, credit if credit else role_type))
		return 1

	@classmethod
	def _scrape_genres(cls, content_id, div):
		pattern = re.compile(r'Genre:')

		div 	= div.find(text=pattern)
		if div is None: return
		div = div.findNext()
		if div is None: return

		text = div.text
		text = cls._strip_whitespace(text)

		genres = text.split(', ')

		# Create multiple genres (split by ', ')
		for genre_string in genres:

			# Create the genre and commit, getting it if it exists already
			genre = data.Genre()
			genre.genre = genre_string
			genre_id = genre.commit()

			if genre_id is None:
				continue

			# Create the genre_movie relationship row
			genre_movie = data.GenreContent()
			genre_movie.content_id 	= content_id
			genre_movie.genre_id 	= genre_id
			genre_movie.commit()

			cls.print_step(2, 'added genre "%s"' % genre_string)

	@classmethod
	def _scrape_trailers(cls, content_id, soup):

		# Extract the json string from the script
		carousel = soup.find('div', {'id': 'videos-carousel-root'})

		if not carousel:
			return 0

		trailers_script = carousel.findNext('script')

		if not trailers_script:
			return 0

		# Cut the string down to just the json text
		trailers_str = trailers_script.text
		trailers_str = trailers_str[:trailers_str.find(';')]
		trailers_str = trailers_str[trailers_str.find('['):trailers_str.rfind(']')+1]

		# Convert the string to json
		try:
			trailers = loads(trailers_str)
		except Exception:
			cls.print_step(2, 'FAILED json.loads()')
			return 0

		# Create each Trailer from the json
		for trailer_json in trailers:
			trailer = data.Trailer()

			trailer.content = content_id
			trailer.runtime = trailer_json.get('duration', 0)

			trailer.name = trailer_json.get('title', None)
			trailer.rtid = trailer_json.get('id', None)
			trailer.url  = trailer_json.get('sourceId', None)

			image_source = trailer_json.get('thumbUrl', None)

			if image_source is not None:
				# Create the image
				image = data.Image()
				image.full = image_source
				image_id = image.commit()
				trailer.image = image_id

			# Save the Trailer
			trailer.commit()

		return len(trailers)

	@classmethod
	def _scrape_photos(cls, content_id, soup):

		# Extract the json string from the script
		carousel = soup.find('div', {'id': 'photos-carousel-root'})

		if not carousel:
			return 0

		photos_script = carousel.findNext('script')

		if not photos_script:
			return 0

		# Cut the string down to just the json text
		photos_str = photos_script.text
		photos_str = photos_str[:photos_str.find(';')]
		photos_str = photos_str[photos_str.find('['):photos_str.rfind(']')+1]

		# Convert the string to json
		try:
			photos = loads(photos_str)
		except Exception:
			cls.print_step(2, 'FAILED json.loads()')
			return 0

		# Add to the movie
		num_photos = 0

		for photo_json in photos:
			photo_url = photo_json.get('urls', {}).get('fullscreen', None)
			photo_icon = photo_json.get('urls', {}).get('thumbnail', None)

			if photo_url is None:
				continue

			photo = data.Image()
			photo.full = photo_url
			photo.icon = photo_icon
			photo.content = content_id
			photo.commit()

			num_photos += 1

		return num_photos

	@classmethod
	def extract(cls, soup):
		return cls._strip_whitespace(soup.string) if soup and soup.string else None

	@classmethod
	def print_step(cls, indent, str):
		print(('.' * 8 * indent) + str)


class MovieScraper(ContentScraper):
	URL = 'm/'
	DATA_CLASS = data.Movie

	@classmethod
	def _run(cls, movie, soup):

		# Scrape movie metadata
		cls.print_step(1, 'metadata')

		content = data.Content()

		poster_image = cls._scrape_metadata(content, movie, soup)

		if content.summary is not None and len(content.summary) > data.Content.SUMMARY_LENGTH:
			content.summary = content.summary[:data.Content.SUMMARY_LENGTH]

		# Parse the json LD schema
		cls.print_step(1, 'reading jsonLdSchema')
		json_string = soup.find('script', {'id': 'jsonLdSchema'}).string

		try:
			json = loads(json_string)
		except Exception:
			cls.print_step(2, 'FAILED json.loads()')
			raise FailedToScrapeContentException

		# Get the extra metadata from the schema
		cls.print_step(1, 'extracting extra metadata')
		movie.rating = json.get('contentRating', None)
		movie.name	 = json.get('name', movie.name)

		# Create the image for the movie
		image = data.Image()
		image.full = json.get('image', None)
		image.icon = poster_image if poster_image else None
		image_id = image.commit()

		# Set the main image for the movie
		content.image 	= image_id
		content.name	= movie.name
		content_id		= content.commit()

		if content_id is None:
			raise FailedToScrapeContentException

		movie.content_id 	= content_id
		movie.commit()

		# Scrape each Genre
		cls.print_step(1, 'genres')
		ptr = soup.find('div', {'id': 'movieSynopsis'})
		ptr = ptr.findNext('ul')
		rtn = cls._scrape_genres(content_id, ptr)

		# Scrape each Trailer
		cls.print_step(1, 'trailers')
		rtn = cls._scrape_trailers(content_id, soup)
		cls.print_step(2, 'found and added %d trailers' % rtn)

		# Scrape all the photos
		cls.print_step(1, 'photos')
		rtn = cls._scrape_photos(content_id, soup)
		cls.print_step(2, 'found and added %d photos' % rtn)

		# Scrape each Actor
		cls.print_step(1, 'actors')
		count = 0
		for actor, credit in zip(json.get('actors', []), json.get('character', [])):
			name 	= actor.get('name', None)
			url 	= actor.get('sameAs', None)
			count += cls._scrape_person(content_id, name, url, 'ACTOR', credit=credit)

			if count == config.ACTORS_LIMIT:
				break

		# Scrape each Director
		cls.print_step(1, 'directors')
		for director in json.get('director', []):
			name 	= director.get('name', None)
			url 	= director.get('sameAs', None)
			cls._scrape_person(content_id, name, url, 'DIRECTOR')

		# Scrape each Author
		cls.print_step(1, 'authors')
		for author in json.get('author', []):
			name 	= author.get('name', None)
			url 	= author.get('sameAs', None)
			cls._scrape_person(content_id, name, url, 'AUTHOR')

		# Scrape each Review and Critic
		cls.print_step(1, 'reviews')
		cls._scrape_reviews(content_id, soup)

		return content_id

	@classmethod
	def _scrape_metadata(cls, content, movie, soup):
		ptr = soup.find('div', {'id': 'movieSynopsis'})

		if ptr is None:
			raise FailedToScrapeContentException

		content.summary = cls.extract(ptr)
		ptr = ptr.findNext('ul')

		# The keys are the string tokens to search for
		# and the values are the key mappings on the movie object
		names = {
			'Genre:': '_', 'Directed By:': '_', 'Written By:': '_',
			'In Theaters:': 'released', 'Box Office:': 'boxOffice',
			'Runtime:': 'runtime', 'Studio:': 'studio', 'Rating:': 'ratingFull',
		}

		# Go through each pair of items in the list
		for item in ptr.findChildren('li'):
			text = cls._strip_whitespace(item.text)

			for name, key in names.items():
				if text.startswith(name):

					# Skip over certain fields that we don't care about right now
					if key == '_':
						break

					# Clean up the string and add it to the movie
					text = text.replace(name, '').strip()
					value = text

					# Convert box office to an integer
					if key == 'boxOffice':
						value = value.replace('$', '').replace(',', '')
						if value.isdigit():
							value = int(value)
						else:
							value = 0

					# Convert runtime to an integer
					if key == 'runtime':
						value = value.split()[0]
						if value.isdigit():
							value = int(value)
						else:
							value = 0

					# Remove 'wide' from release data
					if key == 'released':
						value = value.replace('wide', '').strip()
						value = value.replace('limited', '').strip()
						value = str(datetime.strptime(value, '%b %d, %Y').date())

						content.released = value
						continue

					# Add the attribute to the movie
					setattr(movie, key, value)

		# Get the poster image
		image_section = soup.find('div', {'id': 'movie-image-section'})
		poster_image = image_section.find('img').get('src', None)

		return poster_image

	@classmethod
	def _scrape_reviews(cls, movie_id, soup):
		reviews_section = soup.find('section', {'id': 'contentReviews'})
		reviews_section = reviews_section.find('div', {'id': 'reviews'})

		reviews = reviews_section.findChildren('div', {'class': 'quote_bubble'})

		for review_soup in reviews:

			review_source = review_soup.find('div', {'class': 'review_source media'})
			critic_soup = review_source.findNext('div').findNext('div')

			critic_name = critic_soup.find('a').text
			critic_rtid = critic_soup.find('a').get('href')

			if critic_rtid[-1] == '/':
				critic_rtid = critic_rtid[:-1]

			critic_rtid = critic_rtid.rsplit('/', 1)[-1]

			# Run the scraper
			critic_id = CriticScraper.run(critic_name, critic_rtid, formatted=True)

			if critic_id is None:
				continue

			review = data.Review()

			review.content = movie_id
			review.author = critic_id

			# Get the rating score based on fresh/rotten
			rating_class = review_soup.find('span').get('class')

			if 'fresh' in rating_class:
				review.rating = randint(4, 5)
			elif 'rotten' in rating_class:
				review.rating = randint(1, 3)

			review.publisher = critic_soup.findNext('div').findNext('div').text

			body_soup = review_soup.find('div', {'class': 'media-body'})
			review.body = cls._strip_whitespace(body_soup.find('p').text)

			if review.body is not None and len(review.body) > data.Review.BODY_LENGTH:
				review.body = review.body[:data.Review.BODY_LENGTH]

			line_soup = body_soup.findNext('div')

			source = line_soup.find('a')

			if source is not None:
				review.sourceUrl = source.get('href')

			dateCreated = cls._strip_whitespace(line_soup.text).split(' | ')[0]
			review.dateCreated = str(datetime.strptime(dateCreated, '%B %d, %Y'))

			review.commit()
			cls.print_step(2, 'review by %s' % critic_name)


class ShowScraper(ContentScraper):
	URL = 'tv/'
	DATA_CLASS = data.Show

	@classmethod
	def _run(cls, show, soup):

		# Scrape movie metadata
		cls.print_step(1, 'metadata')

		content = data.Content()

		poster_image = cls._scrape_metadata(content, show, soup)

		if content.summary is not None and len(content.summary) > data.Content.SUMMARY_LENGTH:
			content.summary = content.summary[:data.Content.SUMMARY_LENGTH]

		# Parse the json LD schema
		cls.print_step(1, 'reading jsonLdSchema')
		json_string = soup.find('script', {'id': 'jsonLdSchema'}).string

		try:
			json = loads(json_string)
		except Exception:
			cls.print_step(2, 'FAILED json.loads()')
			raise FailedToScrapeContentException

		# Get the extra metadata from the schema
		cls.print_step(1, 'extracting extra metadata')
		content.name 	= json.get('name', content.name)

		aggregate_rating = json.get('aggregateRating', {})
		show.rating_value = aggregate_rating.get('ratingValue', 0)
		show.rating_count = aggregate_rating.get('ratingCount', 0)

		# Create the image for the movie
		image 		= data.Image()
		image.icon 	= poster_image if poster_image else None
		image_id 	= image.commit()

		# Set the main image for the movie
		content.image 	= image_id
		content.name	= show.name
		content_id		= content.commit()

		if content_id is None:
			raise FailedToScrapeContentException

		show.content_id = content_id
		show.commit()

		# Scrape each Genre
		cls.print_step(1, 'genres')
		ptr = soup.find('section', {'id': 'detail_panel'}).find('table')
		rtn = cls._scrape_genres(content_id, ptr)

		# Scrape each Trailer
		cls.print_step(1, 'trailers')
		rtn = cls._scrape_trailers(content_id, soup)
		cls.print_step(2, 'found and added %d trailers' % rtn)

		# Scrape all the photos
		cls.print_step(1, 'photos')
		rtn = cls._scrape_photos(content_id, soup)
		cls.print_step(2, 'found and added %d photos' % rtn)

		# Scrape each Actor
		cls.print_step(1, 'actors')
		count, total = cls._scrape_actors(content_id, soup)
		cls.print_step(2, 'found %d actors, added %d of them' % (total, count))

		# Scrape each Creator
		cls.print_step(1, 'creators')
		cls._scrape_creators(content_id, soup)

		# Scrape each Producer
		cls.print_step(1, 'producers')
		cls._scrape_producers(content_id, soup)

		# Scrape each Season
		cls.print_step(1, 'seasons')
		rtn = cls._scrape_seasons(content_id, soup)
		cls.print_step(2, 'found and added %d seasons' % rtn)

		return content_id

	@classmethod
	def _scrape_metadata(cls, content, show, soup):

		# Get the summary
		ptr = soup.find('div', {'id': 'movieSynopsis'})
		content.summary = cls.extract(ptr)

		detail = soup.find('section', {'id': 'detail_panel'})

		# Get the network
		pattern = re.compile(r'TV Network:')
		network = detail.find(text=pattern)

		if network is not None:
			network = network.findNext().text
			show.network = cls._strip_whitespace(network)

		# Get the released date
		pattern = re.compile(r'Premiere Date:')
		released = detail.find(text=pattern)

		if released is not None:
			released = released.findNext().text
			released = cls._strip_whitespace(released)
			released = released.replace('wide', '').strip()
			released = released.replace('limited', '').strip()
			released = str(datetime.strptime(released, '%b %d, %Y').date())
			content.released = released

		# Get the last aired date
		ptr = soup.find('p', {'class': 'airDate'})

		if ptr is not None:
			last_aired = ptr.string[ptr.string.find(', ')+2:]
			last_aired = cls._strip_whitespace(last_aired)
			last_aired = str(datetime.strptime(last_aired, '%b %d, %Y').date())
			show.last_aired = last_aired

		# Get the poster image
		image_section = soup.find('div', {'id': 'tv-image-section'})
		poster_image = image_section.find('img').get('src', None)

		return poster_image

	@classmethod
	def _scrape_actors(cls, content_id, soup):

		cast_section = soup.find('div', {'class': 'castSection '})

		if cast_section is None:
			return 0, 0

		cast_members = cast_section.findChildren('div', {'class': 'media-body'})
		count = 0

		for actor in cast_members:
			url_soup = actor.find('a')
			url = url_soup.get('href', None)

			name = cls._strip_whitespace(url_soup.text)

			credit_soup = actor.find('span', {'class': 'characters subtle smaller'})
			credit = 'No Credit'

			if credit_soup is not None:
				credit = credit_soup.text
				credit = cls._strip_whitespace(credit[credit.find(' '):])

			count += cls._scrape_person(content_id, name, url, 'ACTOR', credit=credit)

			if count == config.ACTORS_LIMIT:
				break

		return count, len(cast_members)

	@classmethod
	def _scrape_creators(cls, content_id, soup):

		creators_div = soup.find('div', {'id': 'movieSynopsis'})

		if creators_div is None:
			return

		creators_div = creators_div.findNext()
		creator_urls = creators_div.findChildren('a')

		for creator_url in creator_urls:
			name 	= creator_url.text
			url 	= creator_url.get('href', None)
			cls._scrape_person(content_id, name, url, 'CREATOR')

	@classmethod
	def _scrape_producers(cls, content_id, soup):

		details = soup.find('section', {'id': 'detail_panel'})

		pattern = re.compile(r'Executive Producers:')
		producer_urls = details.find(text=pattern)

		if producer_urls is None:
			return

		producer_urls = producer_urls.findNext().findChildren('a')

		for producer_url in producer_urls:
			name 	= producer_url.text
			url 	= producer_url.get('href', None)
			cls._scrape_person(content_id, name, url, 'PRODUCER')

	@classmethod
	def _scrape_seasons(cls, content_id, soup):
		seasons_section = soup.find('section', {'id': 'seasonList'})

		season_divs = seasons_section.findChildren('div', {'class': 'bottom_divider media seasonItem'})
		season_number = len(season_divs)

		for season_div in season_divs:
			season = data.Season()
			season.show = content_id
			season.number = season_number
			season_number -= 1

			body = season_div.find('div', {'class': 'media-body'})
			name = body.find('a')

			season.name = 'Season ' + str(season.number)

			if name is not None:
				name = name.text
				name = name[name.find(': ')+2:]
				season.name = name

			consensus_div = body.find('div', {'class': 'consensus'})
			consensus = consensus_div.text.replace('Critics Consensus:', '')
			consensus = cls._strip_whitespace(consensus)

			if not consensus == 'No Tomatometer score yet...' \
					and not consensus == 'No consensus yet.':
				season.consensus = consensus

			info_div = body.find('div', {'class': 'season_info'})

			# If there is no info for the season, don't commit this season
			if info_div is None:
				continue

			info = cls._strip_whitespace(info_div.text).split(', ')

			# Get the year and number of episodes
			season.year = int(info[0])

			episodes = info[2].replace('episodes', '')
			episodes = cls._strip_whitespace(episodes)
			season.episodes = int(episodes)

			# Create the poster image for the season
			image_source = season_div.find('img', {}).get('src', None)

			if image_source is not None:
				image 		 = data.Image()
				image.icon 	 = image_source
				image_id 	 = image.commit()
				season.image = image_id

			season.commit()

		return len(season_divs)


class CelebrityScraper(Scraper):
	URL = 'celebrity/'
	DATA_CLASS = data.Celebrity

	@classmethod
	def _run(cls, celeb, soup):
		# Create the Person
		person = data.Person()
		person.name = celeb.name

		bio_div = soup.find('div', {'class':'celeb_summary_bio clamp clamp-5'})

		if bio_div is not None:
			person.bio = bio_div.string.strip()

			if person.bio is not None and len(person.bio) > data.Person.BIO_LENGTH:
				person.bio = person.bio[:data.Person.BIO_LENGTH]

		rows = soup.find_all('div', {'class':'celeb_bio_row'})
		birthday = cls._get_row_value(rows[2].get_text())

		if birthday and birthday != 'Not Available':
			person.birthday = str(datetime.strptime(birthday, '%b %d, %Y').date())

		birthplace = cls._get_row_value(rows[3].get_text())

		if birthplace and birthplace != 'Not Available':
			person.birthplace = birthplace

		image = None

		photos_section = soup.find('section', {'id': 'movie-photos-panel'})

		if photos_section is not None:
			photos_script = photos_section.findNext('script')

			if photos_script is not None:
				photos_str = photos_script.text
				photos_str = photos_str[:photos_str.find(';')]
				photos_str = photos_str[photos_str.find('['):photos_str.rfind(']')+1]

				try:
					photos = loads(photos_str)
					urls = photos[0].get('urls', None)
				except Exception:
					photos = None
					urls = None

				if urls is not None:
					image = data.Image()
					image.full = urls.get('fullscreen', None)
					image.icon = urls.get('thumbnail', None)

		if image is None:
			image = data.Image()
			image_str = soup.find('div', {'class':'celebHeroImage'}).get('style')
			image.full = image_str[image_str.index("'")+1:-2]

		image_id = image.commit()
		person.image = image_id

		# Save the Person
		person_id = person.commit()

		if person_id is None:
			return None

		celeb.person_id = person_id
		celeb.commit()

		return celeb.person_id

	@classmethod
	def _get_row_value(cls, row):
		string = cls._strip_whitespace(row)
		a = string.split(': ')
		return a[1]


def create_empty_celebrity(name):

		person 			= data.Person()
		person.name 	= name
		person_id		= person.commit()

		if person_id is None:
			return None

		celeb 			= data.Celebrity()
		celeb.person_id = person_id
		celeb.name		= name
		celeb.commit()

		return celeb.person_id


class CriticScraper(Scraper):
	URL 		= 'critic/'
	WHITESPACE 	= '-'
	DATA_CLASS	= data.User

	@classmethod
	def _run(cls, user, soup):

		person 		= data.Person()
		person.name = user.name

		want_to_see 			= data.Playlist()
		want_to_see_id 			= want_to_see.commit()
		not_interested 			= data.Playlist()
		not_interested_id 		= not_interested.commit()

		user.email 				= user.rtid + '@rottenoak.com'
		user.password 			= 'X03MO1qnZdYdgyfeuILPmQ'	# hash for "password"
		user.permission	 		= 2							# Critic permission level
		user.views 				= randint(13, 40)
		user.want_to_see 		= want_to_see_id
		user.not_interested 	= not_interested_id

		main 	= soup.find('section', {'id':'criticsSidebar_main'})
		table 	= main.find('table', {'class':'info_list table-dl-like'})
		rows 	= table.findChildren(['th', 'tr'])

		image 		= data.Image()
		image.full 	= main.find('img')['src']
		image_id 	= image.commit()

		person.image = image_id

		for row in rows:
			cells = row.findChildren('td')

			for ki, vi in zip(range(0, len(cells), 2), range(1, len(cells), 2)):
				_k, _v = cells[ki], cells[vi]

				key = cls._format_attribute(_k.string)
				value = [cls._strip_whitespace(x.string) for x in _v.children if x.string is not None]
				value = [x for x in value if x]

				# Standardize to 'bio'
				if key == 'biography':
					key = 'bio'

				if value == 'Not Available':
					value = None

				if value is not None and key == 'birthday':
					value = str(datetime.strptime(value, '%b %d, %Y').date())

				if value is not None and key == 'bio':
					value = value[0] if len(value) > 0 else None

				if hasattr(person, key):
					setattr(person, key, value)
				elif hasattr(user, key):
					setattr(user, key, value)

		# Save the person
		person_id 		= person.commit()

		if person_id is None:
			return None

		user.person_id	= person_id
		user.commit()

		return user.person_id


class SearchScraper(Scraper):
	URL 		= 'search/?search='
	DATA_CLASS  = data.Results
	THRESHOLD 	= 0.60

	# Override super().run() to change the rtid to just the name
	@classmethod
	def run(cls, name, url_name, debug=False, overwrite=False):
		return super().run(name, name, formatted=True, debug=debug, overwrite=overwrite)

	@classmethod
	def _run(cls, results, soup):

		search = soup.find('div', {'id':'search-results-root'})

		if not search:
			results.error = 404
			return results

		script = search.findNext('script')
		json_str = script.string

		for _ in range(2):
			json_str = json_str[json_str.find('{')+1:json_str.rfind('}')]

		try:
			json = loads('{' + json_str + '}')
		except Exception:
			print('SearchScraper: Failed json.loads()')
			return None

		results.movies = json.get('movies', [])
		results.shows = json.get('tvSeries', [])
		return results

	@classmethod
	def match_movie(cls, results, name, year):
		for movie in results.movies:
			res_name = movie.get('name', None)
			res_year = movie.get('year', None)

			if res_name is None or res_year is None or type(res_year) is not int:
				continue

			if cls._similar(res_name, name) >= cls.THRESHOLD \
					and abs(res_year - year) <= 1:
				print('MATCHED with', res_name, res_year)
				res_url = movie.get('url', None)

				if res_url is None:
					return None, None

				rtid = res_url[res_url.find('/m/')+3:]

				return res_name, rtid
		return None, None

	@classmethod
	def match_show(cls, results, name, year):
		for show in results.shows:
			res_name = show.get('title', None)
			res_year = show.get('startYear', None)

			if res_name is None or res_year is None or type(res_year) is not int:
				continue

			if cls._similar(res_name, name) >= cls.THRESHOLD \
					and abs(res_year - year) <= 1:
				print('MATCHED with', res_name, res_year)
				res_url = show.get('url', None)

				if res_url is None:
					return None, None

				rtid = res_url[res_url.find('/tv/')+4:]
				season_index = rtid.rfind('/')

				if season_index >= 0:
					rtid = rtid[:season_index]

				return res_name, rtid
		return None, None

	@classmethod
	def _similar(cls, a, b):
		return SequenceMatcher(None, a, b).ratio()


class ListScraper:

	database = None

	@classmethod
	def run(cls, scraper, items, debug=False):
		''' Run the scraper on a list '''

		# Create the database interface and open the connection
		cls.database = Database(debug=debug)
		data.Data.DEBUG = debug

		errors, count = 0, 0

		for names_list in items:

			name, year = names_list[0], int(names_list[1])
			url_name = name + ' ' + str(year)

			print('Beginning scrape of "%s"' % url_name)

			response = None

			try:
				response = cls._attempt(scraper, names_list, debug=debug)
			except KeyboardInterrupt:
				print('Terminating scraping...')
				break
			except Exception:
				print('Failed Scraper.run("%s")' % url_name)
				traceback.print_exc()
				response = None

				if debug:
					print('Terminating scraping...')
					errors += 1
					break

			if response is None or hasattr(response, 'error'):
				errors += 1
				print('Scraper was unable to find data for "%s"' % name)
			else:
				count += 1
				print('Scraper found data for "%s"' % name)

		# Close the connection
		cls.database.close()

		# Print out statistics for debugging
		success_rate = 100 * (1 - float(errors / len(items)))

		print('-' * 64)
		print('Finished list of %d items with %d%% success rate.'
			% (len(items), success_rate))
		print('Scraped: %d;\tErrors: %d'
			% (count, errors))
		print('-' * 64)

	@classmethod
	def _attempt(cls, scraper, names_list, debug=False):
		name, year = names_list[0], int(names_list[1])
		results = SearchScraper.run(name, name)
		rtid = None

		if results is None:
			return None

		if scraper == MovieScraper:
			name, rtid = SearchScraper.match_movie(results, name, year)
		elif scraper == ShowScraper:
			name, rtid = SearchScraper.match_show(results, name, year)

		if name and rtid:
			return scraper.run(name, rtid,
				formatted=True, debug=debug)

		# Bad attempt num or nothing found
		return None