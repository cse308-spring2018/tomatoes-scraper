import config
from scraper import (
	get_scraper,
	ListScraper,
)

from argparse import ArgumentParser

# Set up command line args and retrieve the args
parser = ArgumentParser()
parser.add_argument("type", help="the type of scraper to run", type=str, choices=["movie", "show"])
parser.add_argument("list_file", help="specify the file to read the list of names from", type=str)
parser.add_argument("-d", "--debug", help="enable debugging", action="store_true")
args = parser.parse_args()

# Read the contents of the file into a list
names = []

with open(args.list_file, 'r') as fin:
	for line in fin:
		cells = line.split(',')
		cells = [' '.join(cell.split()) for cell in cells if not cell.isspace()]
		names.append(cells)

# Get the specified scraper type
scraper = get_scraper(args.type)

# Run the ListScraper on the names
list_scraper = ListScraper()
list_scraper.run(scraper, names, debug=args.debug)