# Tomatoes Scraper

Scrapes data from [Rotten Tomatoes](http://rottentomatoes.com) using Python 3 and BeautifulSoup.

## Installing

Clone the repo and install the dependencies using `pip`:

```sh
$ git clone git@gitlab.com:cse308-spring2018/tomatoes-scraper.git
$ cd tomatoes-scraper
$ pip install bs4
$ pip install mysqlclient
```

## Running

You can now run the scraper by opening the entry program with Python:

```sh
$ python run.py -h
usage: run.py [-h] [-d] list_file
```

You should provide a `*.csv` file that has the format `<movie_name>, <year>`
for each line.

The debug flag (`-d` or `--debug`) will print some extra details while running.
It will also terminate if an error occurs (the normal behavior is to continue to the next movie).

## Updating Oak Database

If there is a change to columns in the tables, you may need to change the
insertion statements in `db.py`. The names in the `VALUES (...)` statements should
match the keys in each class in `data.py`.

## BeautifulSoup4

We are using the latest version of Beautiful Soup to parse the HTML.
Documentation can be found [here](https://www.crummy.com/software/BeautifulSoup/bs4/doc/).