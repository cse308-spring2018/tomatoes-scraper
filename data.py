class Data:
	TABLE = 'errors'
	cache = {}
	id_count = 1

	DEBUG = False

	def __init__(self):
		self.name 	= None
		self.rtid 	= None
		self.id		= None

	def commit(self):
		if hasattr(self, 'error'):
			return None

		if Data.DEBUG:
			return self._commit_cache()
		else:
			return self._commit_db()

	def _commit_cache(self):
		_table = Data.cache.get(self.__class__.TABLE, {})

		_ck = self.candidate_key

		if type(_ck) is list:
			_ck = '-'.join([str(x) for x in _ck])

		_data = _table.get(_ck, None)

		if _data is not None:
			self.id = _data.id
		else:
			if self.rtid is None:
				self.id = Data.id_count
				Data.id_count += 1
			else:
				self.id = self.rtid

		_table[_ck] = self
		print(self.__dict__)
		return self.id

	def _commit_db(self):
		from scraper import ListScraper # resolve circular import

		_id = None
		_ck = self.candidate_key

		if _ck is not None:
			if type(_ck) is not list:
				_ck = [_ck]

			if not None in _ck:
				_id = ListScraper.database.find(self.__class__.TABLE, _ck, isPK=False)

		if _id:
			self.id = _id
		else:
			self.id = ListScraper.database.insert(self.__class__.TABLE, self)

		return self.id

	@property
	def candidate_key(self):
		return [self.rtid]

	@classmethod
	def get(cls, key):
		if Data.DEBUG:
			return cls._get_cache(key)
		else:
			return cls._get_db(key)

	@classmethod
	def _get_db(cls, key):
		from scraper import ListScraper # resolve circular import
		if type(key) is not list:
			key = [key]
		return ListScraper.database.find(cls.TABLE, key, isPK=False)

	@classmethod
	def _get_cache(cls, key):
		_table = Data.cache.get(cls.TABLE, {})
		if type(key) is list:
			key = '-'.join([str(x) for x in key])
		return _table.get(key, None)


class Content(Data):
	TABLE = 'contents'
	SUMMARY_LENGTH = 10000

	def __init__(self):
		super().__init__()

		self.summary	= None
		self.released 	= None
		self.image		= None		# id


class Movie(Data):
	TABLE = 'movies'

	def __init__(self):
		super().__init__()

		self.content_id	= None		# id
		self.rating 	= None
		self.ratingFull = None
		self.boxOffice 	= 0
		self.runtime 	= 0
		self.studio 	= None


class Show(Data):
	TABLE = 'shows'

	def __init__(self):
		super().__init__()

		self.content_id		= None		# id
		self.rating_value	= 0
		self.rating_count	= 0
		self.network		= None
		self.last_aired		= None


class Season(Data):
	TABLE = 'seasons'

	def __init__(self):
		super().__init__()

		self.show			= None		# id
		self.number			= None
		self.year			= None
		self.episodes		= None
		self.consensus		= None
		self.image			= None		# id


class Person(Data):
	TABLE = 'persons'
	BIO_LENGTH = 10000

	def __init__(self):
		super().__init__()

		self.person_id	= None
		self.name 		= None
		self.bio  		= None
		self.birthday 	= None
		self.birthplace = None
		self.image 		= None
		self.dateCreated = 'NOW()'


class Celebrity(Data):
	TABLE = 'celebrities'

	def __init__(self):
		super().__init__()

		self.person_id		= None


class User(Data):
	TABLE = 'users'

	def __init__(self):
		super().__init__()

		self.person_id		= None
		self.email 			= None
		self.password 		= None
		self.active 		= True
		self.verified 		= True
		self.permission 	= 0
		self.views 			= 0
		self.want_to_see 	= None		# id
		self.not_interested = None		# id


class Trailer(Data):
	TABLE = 'trailers'

	def __init__(self):
		super().__init__()

		self.content	= None 		# id
		self.image 		= None		# id
		self.runtime 	= 0


# This class is only used internally as a stub
class Results(Data):

	def __init__(self, movies=None, shows=None):
		self.movies = movies or []
		self.shows = shows or []

	def commit(self):
		return None

	@classmethod
	def get(cls, rtid):
		return None


class Role(Data):
	TABLE = 'roles'

	def __init__(self):
		super().__init__()

		self.content	= None		# id
		self.celebrity 	= None		# id
		self.type 		= None
		self.credit 	= None

	@property
	def candidate_key(self):
		return [self.celebrity, self.content, self.type]


class Review(Data):
	TABLE = 'reviews'
	BODY_LENGTH = 5000

	def __init__(self):
		super().__init__()

		self.content		= None		# id
		self.author 		= None		# id
		self.rating 		= 0
		self.body 			= None
		self.dateCreated 	= None
		self.sourceUrl 		= None

	@property
	def candidate_key(self):
		return [self.author, self.content]


class Image(Data):
	TABLE = 'images'

	NULL_URLS = [
		"https://staticv2-4.rottentomatoes.com/static/images/redesign/poster_default_redesign.gif",
		"https://staticv2-4.rottentomatoes.com/static/images/redesign/actor.default.tmb.gif",
	]

	def __init__(self):
		super().__init__()

		self.content  		= None		# id
		self.full 			= None
		self.icon 			= None
		self.data 			= None
		self.dateType 		= None

	def commit(self):
		if self.full in Image.NULL_URLS:
			return None
		return super().commit()

	'''
	def get_image_blob(self, url):
		if url is None:
			return None

		image_blob = None

		try:
			# Add these back to imports if using this
			from PIL import Image as PILImage
			import requests

			img = PILImage.open(requests.get(url, stream=True).raw)

			with open('image.png', 'wb') as fout:
				img.save(fout)

			with open('image.png', 'rb') as fin:
				image_blob = dumps(str(fin.read()))

			remove('image.png')
		except Exception as ex:
			print(ex)
			print('Failed to download image ID %d url %s' % (self.id, url))
			return None

		return image_blob, 'image/png'
	'''


class Playlist(Data):
	TABLE = 'playlists'


class Genre(Data):
	TABLE = 'genres'

	def __init__(self):
		super().__init__()

		self.genre = None

	@property
	def candidate_key(self):
		return self.genre


class GenreContent(Data):
	TABLE = 'genre_content'

	def __init__(self):
		super().__init__()

		self.genre_id 	= None	# id
		self.content_id = None	# id